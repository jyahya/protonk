Introdução
------------

**ProtonK** é uma remasterização do Proton, o qual usa as últimas versões binárias como base, atualizando o DXVK e o VKD3D sempre para suas últimas versões, assim como adiciona o suporte ao LatencyFlex, para reduzir a latência dos comandos em jogos, melhorando o tempo dos quadros. Ele possui [compilação assíncrona de sombreadores](https://gitlab.com/jyahya/dxvk) (**somente com DXVK**), reduzindo as travadas, melhorando o tempo dos quadros e, em muitos jogos, aumentando a performance.

Instalando
----------------------

1. Baixe [este script](https://gitlab.com/Rbgames_Linux/script-de-instalacao-proton-async)
3. Execute o script no terminal
4. Reinicie o PC
5. Force o *ProtonK* nas opções de compatibilidade na Steam

O script irá automatizar todo o processo, instalará as bibliotecas necessárias, e não será necessário usar parâmetros de inicialização dos jogos na Steam.
Os parâmetros necessários serão setados automaticamente em */etc/environment*.

O ProtonK é baseado nas versões estável e experimental do Proton, respectivamente.

O ProtonK é baseados nos seguintes trabalhos
--------------------------------------------------

[Proton da Valve](https://github.com/ValveSoftware/Proton)

[Proton-GE](https://github.com/GloriousEggroll/proton-ge-custom)

[Minha própria variante do dxvk-git](https://gitlab.com/jyahya/dxvk)

[Minha própria compilação do vkd3d-git](https://gitlab.com/jyahya/vkd3d-proton)

[Middleware usado para melhorar a latência](https://github.com/ishitatsuyuki/LatencyFleX)

## CUIDADO!

**1. O PROTONK FOI FEITO PARA SER USADO NA STEAM, ENTÃO NÃO USE NO LUTRIS OU QUALQUER OUTRA FERRAMENTA EXTERNA A STEAM!**

**2. ESTE PROJETO NÃO TEM QUALQUER AFILIAÇÃO COM A VALVE, SENDO SIMPLESMENTE UMA CUSTOMIZAÇÃO DO PROTON!**

**3. SE TIVER ALGUM PROBLEMA COM O PROTONK, NÃO CONTATE A VALVE, ABRA UM ISSUE AQUI.**

**4. JOGOS MULTIJOGADOR ONLINE PODEM ERRONEAMENTE CONFUNDIR O COMPORTAMENTO E DLLS DO PROTONk COM ALGUM TIPO DE TRAPAÇA!**

## USE POR SUA CONTA E RISCO!

## MEU AGRADECIMENTO A RODRIGO BORGES, O [RB GAMES LINUX](https://www.youtube.com/c/RBGameslinux) PELA DIVULGAÇÃO E AJUDA COM O SCRIPT DE INSTALAÇÃO!

## PROJETO DESCONTINUADO
**ESTE PROJETO ESTÁ DESCONTINUADO.**
**AS VERSÕES JÁ LANÇADAS FICARÃO DISPONÍVEIS ATÉ QUE O SERVIDOR ONDE ESTÃO HOSPEDADAS RESOLVA REMOVÊ-LAS.**
**NENHUMA NOVA VERSÃO SERÁ LANÇADA MAIS!**
**POR FAVOR, NÃO INSISTA EM ME CONTACTAR POR NOVAS VERSÕES!**
**RECOMENDO FORTEMENTE USAREM O [PROTON-GE](https://github.com/GloriousEggroll/proton-ge-custom) NO LUGAR.**

<!-- Target:  GitHub Flavor Markdown.  To test locally:  pandoc -f markdown_github -t html README.md  -->
